/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import {Category as CatIcon, AccountBoxOutlined, AddBox, GifOutlined, AllInboxOutlined, AllInbox} from '@material-ui/icons'
import Dashboard from "@material-ui/icons/Dashboard";
import Settings from '@material-ui/icons/SettingsApplications'
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
// import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
import DashboardPage from "views/admin/Dashboard/Dashboard.js";
import UserProfile from "views/admin/UserProfile/UserProfile.js";
// import TableList from "views/admin/TableList/TableList.js";
// import Typography from "views/admin/Typography/Typography.js";
// import Icons from "views/admin/Icons/Icons.js";
// import Maps from "views/admin/Maps/Maps.js";
import NotificationsPage from "views/admin/Notifications/Notifications.js";
import UpgradeToPro from "views/admin/UpgradeToPro/UpgradeToPro.js";
import Categories from 'views/admin/Categories/Categories'
import Admin from 'views/admin/Admin/Admin'
import Services from 'views/admin/Service/Services';
import Users from 'views/admin/Users/Users';
import Vendors from 'views/admin/Vendors/Vendors';
import Test from 'views/admin/Test';
import Events from 'views/admin/Event/Events';



export const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/admin",
    name: "Admin Managment",
    icon: Settings,
    component: Admin,
    layout: "/admin"
  },
  {
    path: "/categories",
    name: "Category Mangement",
    icon: CatIcon,
    component: Categories,
    layout: "/admin"
  },
  {
    path: "/services",
    name: "Services Mangement",
    icon: AllInbox,
    component: Services,
    layout: "/admin"
  },
  {
    path: "/users",
    name: "Users Management",
    icon: Person,
    component: Users,
    layout: "/admin"
  },
  {
    path: "/vendors",
    name: "Venders Management",
    icon: AccountBoxOutlined,
    component: Vendors,
    layout: "/admin"
  },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin"
  // },
  {
    path: "/events",
    name: "View Events",
    icon: Unarchive,
    component: Events,
    layout: "/admin"
  }
];

// export default dashboardRoutes;

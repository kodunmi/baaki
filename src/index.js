import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Redirect, Router } from "react-router-dom";
import { ApolloClient, InMemoryCache, createHttpLink } from '@apollo/client';
import { ApolloProvider } from '@apollo/client';
import "assets/css/material-dashboard-react.css?v=1.9.0";
import App from "App";
import Cookies from "universal-cookie";
import { setContext } from '@apollo/client/link/context';

const hist = createBrowserHistory();
const cookies = new Cookies();
const httpLink = createHttpLink({
  uri: 'https://apibeeki.mpanaka.net'
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const user = cookies.get('user')
  if (!user) return <Redirect to={{ pathname: '/login' }} /> 
  const token = user.token
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});


const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <Router history={hist}>
        <App/>
    </Router>
  </ApolloProvider>
  ,
  document.getElementById("root")
);

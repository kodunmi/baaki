import {
    grayColor,
    primaryColor,
    infoColor,
    successColor,
    warningColor,
    dangerColor,
    roseColor,
    whiteColor,
    blackColor,
    hexToRgb
  } from "assets/jss/material-dashboard-react.js";

  import bg from 'assets/img/bg/bg-01.jpg'

  const loginPageStyle = {
    background: {
        backgroundImage: `url(${bg})`,
        height: '100vh',
        width: '100vw',
        position: 'fixed',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: 'cover'
    }
  }

  export default loginPageStyle;
import { gql, useQuery } from '@apollo/client';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import MaterialTable from 'material-table';
import React from 'react'
import {useHistory } from 'react-router-dom';

const GET_VENDORS = gql`
  query{
    getbasedonusertype(role:"VENDOR"){
        id
        lname
        fname
        email
        phone_number
        role
    }
  }
`

const Vendors = ({match}) => {
    const history = useHistory()
    const {loading, error, data} = useQuery(GET_VENDORS,{
        pollInterval: 500,
    })

    const vendorsData = data ? data.getbasedonusertype.map(({id , lname, fname, email,phone_number, role}) =>{
        const vendor =  {
            id,
            lname,
            fname,
            email,
            phone_number,
            role
        }
    
        return vendor
    }) : []

console.log(vendorsData);
    
    return (
        <div>
            <GridContainer className="mt-5">
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="main">Vendors</CardHeader>
                        <MaterialTable
                            title="All Vendors"
                            columns={[
                                { title: 'id', field: 'id'},
                                { title: 'Last Name', field: 'lname' },
                                { title: 'First Name', field: 'fname' },
                                { title: 'Email', field: 'email'},
                                { title: 'phone_number', field: 'phone_number'},
                                { title: 'Role', field: 'role'},
                            ]}
                            data={vendorsData}
                            
                            actions={[
                                rowData => ({
                                    icon: 'visibility',
                                    tooltip: `View ${rowData.firstname}`,
                                    onClick: ((event, rowData) => history.push(`${match.url}/${rowData.id}`))
                                }),
                            ]}
                            options={{
                                actionsColumnIndex: -1,
                                exportAllData: true,
                                filtering: true,
                                exportButton: {
                                    csv: true,
                                    pdf: true,
                                }
                            }}
                        />
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}

export default Vendors

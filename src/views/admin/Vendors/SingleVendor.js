import React from 'react'
import Icon from "@material-ui/core/Icon";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import Store from "@material-ui/icons/Store";
import styles from "assets/jss/views/dashboardStyle.js";
import { IconButton, List, ListItem, ListItemSecondaryAction, ListItemText, makeStyles, Table } from '@material-ui/core';
import avatar from "assets/img/faces/marc.jpg";
import CardAvatar from 'components/Card/CardAvatar';
import CardBody from 'components/Card/CardBody';
import Button from "components/CustomButtons/Button.js";
import { grayColor } from 'assets/jss/material-dashboard-react'
import { BugReport, Cloud, Code, Send } from '@material-ui/icons';
import CustomTabs from 'components/CustomTabs/CustomTabs';
import Tasks from 'components/Tasks/Tasks';
import swal from 'sweetalert';
import { gql, useMutation, useQuery } from '@apollo/client';
import { SyncLoader } from 'react-spinners';

const useStyles = makeStyles(styles);
const GET_VENDOR = gql`
    query ($id: Int){
    userfind(id: $id){
        id
        fname
        lname
        email
        vendor_validity
    }
}
`
const VERIFY_VENDOR = gql`
    mutation($id: Int, $vendor_validity: Boolean){
        vendor_verify(id:$id, vendor_validity: $vendor_validity){
            fname
            lname
        }
    }
`
function verify(mutation){
    mutation.then(data=> swal("vender verified", `you have successfully unverified ${data.userfind.fname}`, "success"))  
}
const SingleVendor = ({ match: { params } }) => {
    const classes = useStyles();
    const { loading, error, data } = useQuery(GET_VENDOR, {
        pollInterval: 500,
        variables: { id: Number(params.id) }
    })
    const [verifyVendor] = useMutation(VERIFY_VENDOR)

    return (
        <div>
        {loading ? <SyncLoader/> : error ? "error loading data" : (
            <>
            <GridContainer>
                <GridItem xs={12} sm={6} md={4}>
                    <Card>
                        <CardHeader color="warning" stats icon>
                            <CardIcon color="warning">
                                <Store />
                            </CardIcon>
                            <p className={classes.cardCategory}>Services Offer</p>
                            <h3 className={classes.cardTitle}>
                                0
                            </h3>
                        </CardHeader>
                    </Card>
                </GridItem>

                <GridItem xs={12} sm={6} md={4}>
                    <Card>
                        <CardHeader color="danger" stats icon>
                            <CardIcon color="danger">
                                <Icon>date_range</Icon>
                            </CardIcon>
                            <p className={classes.cardCategory}>Events Request</p>
                            <h3 className={classes.cardTitle}>0</h3>
                        </CardHeader>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={6} md={4}>
                    <Card>
                        <CardHeader color="info" stats icon>
                            <CardIcon color="info">
                                <Icon>payments</Icon>
                            </CardIcon>
                            <p className={classes.cardCategory}>Payments Recieved</p>
                            <h3 className={classes.cardTitle}>2000</h3>
                        </CardHeader>
                    </Card>
                </GridItem>
            </GridContainer>
            <GridContainer xs={12} sm={12} md={12}>
                <GridItem xs={12} sm={12} md={6}>
                    <Card profile>
                        <CardAvatar profile>
                            <a href="#pablo" onClick={e => e.preventDefault()}>
                                <img src={avatar} alt="..." />
                            </a>
                        </CardAvatar>
                        <CardBody profile>
                            <h6>{data.userfind.fname} {data.userfind.lname}</h6>
                            <h4 >{data.userfind.email}</h4>
                            <p>{data.userfind.fname} is {data.userfind.vendor_validity ? 'verified': 'unverified'} click below to</p>
                            {data.userfind.vendor_validity ? (
                                <Button color="primary" round onClick={() => verify(verifyVendor({variables: {id:data.userfind.id,vendor_validity:'True' } }))}>
                                    Unverified
                                </Button>
                            ) : (
                                    <Button color="primary" round onClick={() => swal("vender verified", `you have successfully verified ${data.userfind.fname}`, "success")}>
                                        Verify
                                    </Button>
                                )}

                        </CardBody>
                    </Card>
                </GridItem>
                {/* <GridItem xs={12} sm={12} md={6}>
                    {
                        user[0].events.map(({ date, name, services }) => {
                            return (
                                <GridItem>
                                    <Card>
                                        <CardHeader color="danger" stats icon>
                                            <CardIcon color="danger">
                                                <Icon>date_range</Icon>
                                            </CardIcon>
                                            <h3 style={{ color: grayColor[0] }}>{name}</h3>
                                            <Button color='primary' round>View event</Button>
                                            <Button color='info' round>View user</Button>
                                        </CardHeader>
                                    </Card>
                                </GridItem>
                            )
                        })
                    }
                </GridItem> */}
            </GridContainer>
            {/* <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <CustomTabs
                        title="Vendor events acceptance"
                        headerColor="primary"
                        tabs={[
                            {
                                tabName: "All Events",
                                tabIcon: BugReport,
                                tabContent: (
                                    <List>
                                        {[0, 1, 2, 3].map((value) => {
                                            return (
                                                <ListItem key={value} dense button>
                                                    <ListItemText id={value} primary={`Event ${value + 1}`} />
                                                    <ListItemSecondaryAction>
                                                        <IconButton edge="end" aria-label="view">
                                                            <Send/>
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            );
                                        })}
                                    </List>
                                )
                            },
                            {
                                tabName: "Accepted Events",
                                tabIcon: Code,
                                tabContent: (
                                    <h2>accepted events</h2>
                                )
                            },
                            {
                                tabName: "Rejected Events",
                                tabIcon: Cloud,
                                tabContent: (
                                    <h2>rejected events</h2>
                                )
                            }
                        ]}
                    />
                </GridItem>
            </GridContainer> */}
            </>
            )}
        </div>
    )
}

export default SingleVendor


import React, { useState } from 'react'
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import MaterialTable from 'material-table';
import gql from "graphql-tag";
import SyncLoader from "react-spinners/SyncLoader";
import { useMutation, useQuery } from "@apollo/client";
import swal from 'sweetalert';
import tw from 'twin.macro';

const GET_CAT = gql`
    query {
        categories{
        id
        name
        description
    } 
}       
`
const ADD_CAT = gql`
    mutation ($name: String!, $description: String){
        addcategory(name: $name, description: $description){
            name
        }
    }
`
const DELETE_CAT = gql`
    mutation ($id: Int){
        deletecategory(id:$id){
            name
        }
    }
`
const UPDATE_CAT = gql`
    mutation ($id: Int, $name: String!, $description: String){
        upsertcategory(id: $id, name:$name, description: $description){
            name
        }
    }
`
const Loader = tw(SyncLoader)`
    my-5
`

const Category = () => {
    const [addCategory] = useMutation(ADD_CAT)
    const [updateCategory] =useMutation(UPDATE_CAT)
    const [deleteCategory] =useMutation(DELETE_CAT)
    const { loading, error, data } = useQuery(GET_CAT,{
        pollInterval: 500,
    })

    
    
    const cat = data ? data.categories.map(({name, id, description}) => {
       return  {id ,name, description}
    }) : []
    
    return (
        <div>
            <GridContainer className="mt-5">
                <GridItem xs={12} sm={12} md={12}>
                    {loading ? <Loader/> : error ? "error loading data" : ""}
                   
                    <Card>
                        <CardHeader color="main">Categories</CardHeader>
                        
                            <MaterialTable
                            title="All Categories"
                            columns={[
                                { title: 'Id', field: 'id', editable:'never', type:"numeric" },
                                { title: 'Name', field: 'name' },
                                { title: 'Description', field: 'description' },
                            ]}
                            data={cat}
                            editable={{
                                onRowAdd: newData => addCategory({variables: {name: newData.name, description: newData.description}})
                                .then(data =>{
                                    console.log(data);
                                    swal("success", `${data.data.addcategory.name} added successfully`, "success");
                                })
                                .catch(error =>{
                                    swal("could not add", `${error}`, "error");
                                })
                                ,

                                onRowUpdate:(newData, oldData) => updateCategory({variables: {id: Number(oldData.id), name: newData.name, description: newData.description}})
                                .then(data =>{
                                    swal("success", `${oldData.name} updated to ${data.data.upsertcategory.name} successfully`, "success");
                                })
                                .catch(error=>{
                                    swal("could not update", `${error}`, "error");
                                }),

                                onRowDelete: (oldData) => deleteCategory({variables:{id: Number(oldData.id)}})
                                .then(data =>{
                                    swal("success", `${data.data.deletecategory.name} deleted successfully`, "success");
                                })
                                .catch(error=>{
                                    swal("could not delete", `${error}`, "error");
                                }),

                            }}
                            actions={[
                                rowData => ({
                                    icon: 'add',
                                    tooltip: `Add new service to ${rowData.name}`,
                                    onClick: ((event, rowData) => {
                                        
                                    })
                                }),
                            ]}
                            options={{
                                actionsColumnIndex: -1,
                                exportAllData: true,
                                filtering: true,
                                exportButton: {
                                    csv: true,
                                    pdf: true,
                                }
                            }}
                        />                        
                    </Card>
                </GridItem>
            </GridContainer>
            
        </div>
    )
}

export default Category

import React from 'react'
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import MaterialTable from 'material-table';
import SyncLoader from "react-spinners/SyncLoader";
import gql from "graphql-tag";
import { useMutation, useQuery } from "@apollo/client";
import swal from 'sweetalert';

const ADD_ADMIN = gql`
mutation ($email: String! , $password: String!, $fname:String, $lname: String, $role: String!, $phone_number: String){
    signup(email: $email, password: $password, fname: $fname, lname: $lname, role: $role, phone_number: $phone_number){
      user{
        fname
        lname
      }
      token
    }
  }
`
const GET_ADMIN = gql`
  query{
    getbasedonusertype(role:"ADMIN"){
      lname
      fname
      email
      phone_number
    }
  }
`

const Admin = () => {
  const [addAdmin] = useMutation(ADD_ADMIN)
  const {loading, error, data} = useQuery(GET_ADMIN,{
    pollInterval: 500,
  })

const allAdmin = data ? data.getbasedonusertype.map(({fname, lname,email, phone_number})=>{
  return {fname, lname, email, phone_number}
}) : []

  console.log(allAdmin);
  return (
    <div>
      <GridContainer className="mt-5">
        <GridItem xs={12} sm={12} md={12}>
          {loading ? <SyncLoader/> : error ? "error loading data" : ""}        
          <Card>
            <CardHeader color="main">Admins</CardHeader>
              <MaterialTable
                title="All admin"
                columns={[
                  { title: 'First Name', field: 'fname'},
                  { title: 'Password', field: 'password', initialEditValue: 'password'},
                  { title: 'Last Name', field: 'lname' },
                  { title: 'Email', field: 'email'},
                  { title: 'Phone', field: 'phone_number'},
                ]}
                data={allAdmin}
                editable={{
                  onRowAdd: newData => addAdmin({variables: {email: newData.email, password: newData.password, lname: newData.lname , fname: newData.fname, phone_number: newData.phone_number, role: "ADMIN"}}).then(data =>{
                    swal("Admin created", `${data.data.signup.user.fname} created successfully`, "success");
                  }).catch(error =>{
                    swal("error creating admin", `${error}`, "error");
                  }),

                  // onRowAdd: newData => {
                  //   newData['role']='ADMIN'
                  //   console.log(newData)
                  // },
                  // onRowAdd: newData => {try {
                  //   addAdmin({variables: newData})
                  // } catch (error) {
                  //   console.log(error);
                  // }},
                    
                  onRowUpdate:(newData, oldData) =>
                      new Promise((resolve, reject) => {
                          setTimeout(() => {
                              {
                                  
                              }
                              resolve()
                          }, 1000)
                      }),
                  

                  onRowDelete: (oldData) => {

                  }

              }}
                options={{
                  actionsColumnIndex: -1,
                  exportAllData: true,
                  filtering: true,
                  exportButton: {
                      csv: true,
                      pdf: true,
                  }
                }}
              />
            
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  )
}

export default Admin

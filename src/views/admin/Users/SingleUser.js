import React from 'react'
import Icon from "@material-ui/core/Icon";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import Store from "@material-ui/icons/Store";
import styles from "assets/jss/views/dashboardStyle.js";
import { makeStyles } from '@material-ui/core';
import avatar from "assets/img/faces/marc.jpg";
import CardAvatar from 'components/Card/CardAvatar';
import CardBody from 'components/Card/CardBody';
import Button from "components/CustomButtons/Button.js";
import { grayColor } from 'assets/jss/material-dashboard-react'
import swal from 'sweetalert';
import { gql, useQuery } from '@apollo/client';
import { SyncLoader } from 'react-spinners';

const useStyles = makeStyles(styles);

const GET_USER = gql`
    query ($id: Int){
    userfind(id: $id){
        fname
        lname
        email
    }
}
`

const SingleUser = ({ match: { params } }) => {
    const classes = useStyles();
    const { loading, error, data } = useQuery(GET_USER, {
        pollInterval: 500,
        variables: { id: Number(params.id) }
    })

    return (
        <div>
            {loading ? <SyncLoader/> : error ? "error loading data" : (
                <>
                    <GridContainer>
                        <GridItem xs={12} sm={6} md={4}>
                            <Card>
                                <CardHeader color="warning" stats icon>
                                    <CardIcon color="warning">
                                        <Store />
                                    </CardIcon>
                                    <p className={classes.cardCategory}>Services Required</p>
                                    <h3 className={classes.cardTitle}>
                                        0
                                    </h3>
                                </CardHeader>
                            </Card>
                        </GridItem>

                        <GridItem xs={12} sm={6} md={4}>
                            <Card>
                                <CardHeader color="danger" stats icon>
                                    <CardIcon color="danger">
                                        <Icon>date_range</Icon>
                                    </CardIcon>
                                    <p className={classes.cardCategory}>Events</p>
                                    <h3 className={classes.cardTitle}>0</h3>
                                </CardHeader>
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={4}>
                            <Card>
                                <CardHeader color="info" stats icon>
                                    <CardIcon color="info">
                                        <Icon>payments</Icon>
                                    </CardIcon>
                                    <p className={classes.cardCategory}>Payments</p>
                                    <h3 className={classes.cardTitle}>2000</h3>
                                </CardHeader>
                            </Card>
                        </GridItem>
                    </GridContainer>
                    <GridContainer xs={12} sm={12} md={12}>
                        <GridItem xs={12} sm={12} md={6}>
                            <Card profile>
                                <CardAvatar profile>
                                    <a href="#pablo" onClick={e => e.preventDefault()}>
                                        <img src={avatar} alt="..." />
                                    </a>
                                </CardAvatar>
                                <CardBody profile>
                                    <h6>{data.userfind.fname} {data.userfind.lname}</h6>
                                    <h4 >{data.userfind.email}</h4>
                                    {/* <p>{user[0].firstname} is {user[0].verification} click below to</p>
                            {user[0].verification == 'enabled' ? (
                                <Button color="main" round onClick={() => swal("vender enabled", `you have successfully enabled ${user[0].firstname}`, "success")}>
                                    Disable
                                </Button>
                            ):(
                                <Button color="main" round onClick={() => swal("vender disable", `you have successfully disable ${user[0].firstname}`, "success")}>
                                    Enable
                                </Button>
                            )} */}

                                </CardBody>
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={6}>
                            {/* {
                        user[0].events.map(({date, name, services}) => {
                            return(
                            <GridItem>
                                <Card>
                                    <CardHeader color="danger" stats icon>
                                        <CardIcon color="danger">
                                            <Icon>date_range</Icon>
                                        </CardIcon>
                                        <h3 style={{color: grayColor[0]}}>{name}</h3>
                                        <p className={classes.cardTitle}>{name} has {services.length} services</p>
                                    </CardHeader>
                                </Card>
                            </GridItem>
                            )
                        })
                    } */}
                        </GridItem>
                    </GridContainer>
                </>
            )}

        </div>
    )
}

export default SingleUser

import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import MaterialTable from 'material-table';
import React from 'react'
import {useHistory } from 'react-router-dom';
import gql from "graphql-tag";
import {useQuery} from "@apollo/client";
import { SyncLoader } from 'react-spinners';

const GET_USERS = gql`
  query{
    getbasedonusertype(role:"USER"){
        id
        lname
        fname
        email
        phone_number
    }
  }
`

const Users = ({match}) => {
    const history = useHistory()
    const {loading, error, data} = useQuery(GET_USERS,{
        pollInterval: 500,
    })

const usersData = data ? data.getbasedonusertype.map(({id , lname, fname, email,phone_number}) =>{
    const user =  {
        id,
        lname,
        fname,
        email,
        phone_number
    }

    return user
}) : []
    
    // console.log(newUser);
    return (
        <div>
            <GridContainer className="mt-5">
            {loading ? <SyncLoader/> : error ? "error loading data" : ""}  
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="main">Users</CardHeader>
                        <MaterialTable
                            title="All Users"
                            columns={[
                                { title: 'id', field: 'id'},
                                { title: 'Last Name', field: 'lname' },
                                { title: 'First Name', field: 'fname' },
                                { title: 'Email', field: 'email'},
                                { title: 'phone_number', field: 'phone_number'},
                            ]}
                            data={usersData}
                            
                            actions={[
                                rowData => ({
                                    icon: 'visibility',
                                    tooltip: `View ${rowData.fname}`,
                                    onClick: ((event, rowData) => history.push(`${match.url}/${rowData.id}`))
                                }),
                            ]}
                            options={{
                                actionsColumnIndex: -1,
                                exportAllData: true,
                                filtering: true,
                                exportButton: {
                                    csv: true,
                                    pdf: true,
                                }
                            }}
                        />
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}

export default Users

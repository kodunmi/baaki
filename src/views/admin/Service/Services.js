import React from 'react'
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import {AccountBoxOutlined, Visibility } from '@material-ui/icons'
import MaterialTable from 'material-table';
import {useHistory } from 'react-router-dom';
import CustomDialog from 'components/CustomDialog/CustomDialog';
import { users } from '../../../user'
import SyncLoader from "react-spinners/SyncLoader";
import { Avatar, Chip, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import { useMutation, useQuery } from "@apollo/client";
import gql from "graphql-tag";
import tw from 'twin.macro';
import swal from 'sweetalert';

const GET_CAT = gql`
    query {
        categories{
        id
        name
        description
    } 
}       
`

const GET_SERVICE = gql`
    query{
        services{
        id
        name
        description
        category_id
    }
}
`
const ADD_SERVICE = gql`
    mutation ($name: String!, $description: String, $category_id: Int, $others: String){
        addsevice(name: $name, description: $description, category_id: $category_id, others: $others){
            name
        }
    }
`

const UPDATE_SERVICE = gql`
    mutation ($id:Int $name: String!, $description: String, $category_id: Int, $others: String){
        upsertservice(id:$id, name: $name, description: $description, category_id: $category_id, others: $others){
            name
        }
    }
`

const DELETE_SERVICE = gql`
    mutation($id: Int){
        deleteservice(id: $id){
        name
    }
}
`


const Loader = tw(SyncLoader)`
    my-5
`

const Services = ({match}) => {    
    const history = useHistory()
    const [open, setOpen] = React.useState(false)
    const [sellectedService, setSellectedService] = React.useState({})
    const { loading: catLoading, error: catError, data: catData } = useQuery(GET_CAT,{
        pollInterval: 500,
    })
    const { loading: serviceLoading, error: serviceError, data: serviceData } = useQuery(GET_SERVICE,{
        pollInterval: 500,
    })
    const [addService] = useMutation(ADD_SERVICE)
    const [updateService] = useMutation(UPDATE_SERVICE)
    const [deleteService] = useMutation(DELETE_SERVICE)

    // const catArray = catData ? catData.categories.map(({name, id}) => {
    //     return  {...[Number(id)] : name}
    //  }) : {}

    //  const cat = {...catArray}

    const cat = catData ? catData.categories.reduce((acc, cat) => (
        acc[cat.id] = cat.name, acc
    ), {}) : {}

    const allServices = serviceData ? serviceData.services.map(({id, name, description,category_id}) =>{
        return {id, name,description, category_id}
    }) : []


    console.log(allServices, cat)

    const handleClickUsersOpen = () => {
        setOpen(true);
    };

    const handleUserClose = () => {
        setOpen(false);
    };


    const viewUsers=(data)=>{
        setSellectedService({})
        handleClickUsersOpen()
        setSellectedService(data)
    } 

    // console.log(catData,serviceData);
    return (
        <div>
            <CustomDialog open={open} handleClose={handleUserClose} title={`view all users/vendor for ${sellectedService.name}`} scroll='paper'>
                <List dense>
                    {users.map((value) => {
                        const labelId = `checkbox-list-secondary-label-${value.id}`;
                        return (
                        <ListItem key={value.id} button>
                            <ListItemAvatar>
                                <Avatar
                                    alt={`Avatar n°${value.id}`}
                                    src={`https://ui-avatars.com/api/?name=${value.firstname}+${value.lastname}&background=9c27b0&color=fff`}
                                />
                            </ListItemAvatar>
                            <ListItemText id={labelId} primary={`${value.firstname} ${value.lastname}`} />
                            <ListItemSecondaryAction>
                            <Chip
                                color="primary"
                                avatar={<Avatar><Visibility/></Avatar>}
                                label="view"
                                onClick={() => {history.push(`/admin/users/${value.id}`)}}
                                variant="outlined"
                            />
                            </ListItemSecondaryAction>
                        </ListItem>
                        );
                    })}
                </List>
            </CustomDialog>
            <GridContainer className="mt-5">
                <GridItem xs={12} sm={12} md={12}>
                { serviceLoading || catLoading ? <Loader/> : serviceError || catError ? "error loading data" : ""}
                    <Card>
                        <CardHeader color="main">Services</CardHeader>
                       
                        <MaterialTable
                            title="All Services"
                            columns={[
                                { title: 'id', field: 'id', editable:'never'},
                                { title: 'Name', field: 'name' },
                                { title: 'Description', field: 'description' },
                                { title: 'Categories', field: 'category_id', lookup: cat},
                                // { title: 'Events', field: 'events', type: 'numeric', editable:'never' },
                                // { title: 'Vendors', field: 'vendors', type: 'numeric', editable:'never' },
                            ]}
                            data={allServices}
                            editable={{
                                onRowAdd: newData => addService({variables: {name: newData.name, description: newData.description, category_id: Number(newData.category_id) }})
                                .then(data =>{
                                    swal("service created", `${data.data.addsevice.name} created successfully`, "success");
                                    
                                })
                                .catch(error =>{
                                    swal("error creating service", `${error}`, "error");
                                })
                                ,

                                onRowUpdate:(newData, oldData) => updateService({variables: {id: Number(oldData.id), name: newData.name, description: newData.description, category_id: Number(newData.category_id)}})
                                .then(data=>{
                                    swal("service update", `${oldData.name} updated successfully`, "success");
                                })
                                .catch(error=>{
                                    swal("error creating service", `${error}`, "error");
                                })
                                ,
                                onRowDelete: (oldData) => deleteService({variables: {id: Number(oldData.id)}})
                                .then(data =>{
                                    swal("service deleted", `${oldData.name} deleted successfully`, "success");
                                })
                                .catch(error =>{
                                    swal("error deleting service", `${error}`, "error");
                                })

                            }}
                            actions={[
                                rowData => ({
                                    icon: AccountBoxOutlined,
                                    tooltip: `view all vendors for ${rowData.name}`,
                                    onClick: ((event, rowData) => {
                                        viewUsers(rowData)
                                    })
                                }),
                                rowData => ({
                                    icon: 'person',
                                    tooltip: `view all users for ${rowData.name}`,
                                    onClick: ((event, rowData) => {
                                        viewUsers(rowData)
                                    })
                                }),
                                // rowData => ({
                                //     icon: 'visibility',
                                //     tooltip: `View ${rowData.name}`,
                                //     onClick: ((event, rowData) => history.push(`${match.url}/${rowData.name}`))
                                // }),
                            ]}
                            options={{
                                // selection: true,
                                draggable: true,
                                actionsColumnIndex: -1,
                                exportAllData: true,
                                filtering: true,
                                exportButton: {
                                    csv: true,
                                    pdf: true
                                }
                            }}
                        />

                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}

export default Services

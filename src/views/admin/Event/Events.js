import React from 'react'
import { gql, useQuery } from '@apollo/client'
import GridContainer from 'components/Grid/GridContainer'
import { SyncLoader } from 'react-spinners'
import GridItem from 'components/Grid/GridItem'
import Card from 'components/Card/Card';
import CardHeader from 'components/Card/CardHeader';
import MaterialTable from 'material-table'
import { useHistory } from 'react-router-dom'

const GET_EVENTS = gql`
  query{
    events{
        id
        event_name
        event_type
        event_date
    }
  }
`

const Events = ({match}) => {
    const history = useHistory()
    const {loading, error, data: eventsData} = useQuery(GET_EVENTS,{
        pollInterval: 500,
    })
    const newEventData = eventsData ? eventsData.events.map(({id, event_name,event_type, event_date}) =>{
        const event =  {
            id,
            event_name,
            event_type,
            event_date
        }
    
        return event
    }) : []

    return (
        <div>
            <GridContainer className="mt-5">
            {loading ? <SyncLoader/> : error ? "error loading data" : ""}  
                <GridItem xs={12} sm={12} md={12}>
                    <Card>
                        <CardHeader color="main">EVENTS</CardHeader>
                        <MaterialTable
                            title="All Events"
                            columns={[
                                { title: 'id', field: 'id'},
                                { title: 'Name of event', field: 'event_name'},
                                { title: 'Type of event', field: 'event_type)' },
                                { title: 'Date of event', field: 'event_date' },
                            ]}
                            
                            data={newEventData}
                            
                            actions={[
                                rowData => ({
                                    icon: 'visibility',
                                    tooltip: `View ${rowData.event_name}`,
                                    onClick: ((event, rowData) => history.push(`${match.url}/${rowData.id}`))
                                }),
                            ]}
                            options={{
                                actionsColumnIndex: -1,
                                exportAllData: true,
                                filtering: true,
                                exportButton: {
                                    csv: true,
                                    pdf: true,
                                }
                            }}
                        />
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}

export default Events

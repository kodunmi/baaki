import React from 'react'
import { Redirect } from 'react-router-dom'
import Cookies from 'universal-cookie';


class ProtectedRoute extends React.Component {
    render() {
        const cookies = new Cookies();

        if (cookies.get('user')) {
            const user = cookies.get('user');
            const usertype = user.userType;
            const Component = this.props.component;
            const isAuthenticated = user;

            if (isAuthenticated) {
                if (usertype.toLowerCase() === this.props.type.toLowerCase()) {
                    return <Component />
                } else {
                    let route;
                    switch (usertype.toLowerCase()) {
                        case 'admin':
                            route = "/admin/dashboard";
                            break;
                        case 'vendor':
                            route = "/vendor/dashboard";
                            break;
                        case 'user':
                            route = "/user/dashboard";
                    }

                    return <Redirect to={route} />
                }
            } else {
                return <Redirect to={{ pathname: '/login' }} />
            }
        } else {
            return <Redirect to={{ pathname: '/login' }} />
        }

    }
}

export default ProtectedRoute;
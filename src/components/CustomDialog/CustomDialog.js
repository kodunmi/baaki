import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from 'prop-types'

const styles = (theme) => ({
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },

    title:{
        marginRight: theme.spacing(10)
    }
  });
const CustomDialog = ({open, handleClose, title,text,children, ...rest}) => {
  const useStyles = makeStyles(styles);
  const classes = useStyles();
  return (
    <div>
      <Dialog 
        {...rest}
        open={open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title" className={classes.title}>{title}
            {handleClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {text}
          </DialogContentText>
          {children}
        </DialogContent>
      </Dialog>
    </div>
  );
}

CustomDialog.prototype ={
    open: PropTypes.bool.isRequired, 
    handleClose: PropTypes.func.isRequired, 
    title: PropTypes.string,
    text: PropTypes.string,
    children: PropTypes.element.isRequired,
    button: PropTypes.element
}
export default CustomDialog
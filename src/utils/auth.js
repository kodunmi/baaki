import React from 'react'
import { Redirect, useHistory } from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies()
export const useLogout = () =>{
    const history = useHistory()
    return () => {
        cookies.remove('user', {
            path: '/'
        })
        history.push('/login')
    }
    
}

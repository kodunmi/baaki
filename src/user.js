export const users = [
    {
        'id': 1,
        'firstname': 'tayo',
        'lastname': 'brands',
        'email': 'brands@gmail.com',
        'verification': 'enabled',
        'events': [
            {
                'name': 'wedding party',
                'date': Date.now,
                'services': [1,3.3,4]
            },
            {
                'name': 'birthday party',
                'date': Date.now,
                'services': [1, 3, 3, 4]
            },
            {
                'name': 'birthday party',
                'date': Date.now,
                'services': [1, 3,]
            },
            
        ],
    },    
    {
        'id': 2,
        'firstname': 'lekan',
        'lastname': 'kodunmi',
        'email': 'lekan@gmail.com',
        'verification': 'disabled',
        'events': [
            {
                'name': 'church party',
                'date': Date.now,
                'services': [1, 3, 3, 4]
            },
            {
                'name': 'launch party',
                'date': Date.now,
                'services': [1, 3, 3, 4]
            },
            
        ],
    },    
]
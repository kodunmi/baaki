import React from "react";
import { GlobalStyles } from 'twin.macro'
import {Redirect,Route, Switch} from "react-router-dom";
import Admin from "layouts/Admin.js";
import RTL from "layouts/RTL.js";
import LoginPage from 'pages/admin/LoginPage'
import "assets/css/material-dashboard-react.css?v=1.9.0";
import Test from "pages/test";
// import Login from "pages/Login.js";
import Login from "pages/LoginPage.js";
import Register from "pages/Register.js";
import ProtectedRoute from "components/ProtectedRoute";
import { useLogout } from "utils/auth";


const App = () => {
    const logout = useLogout()
    return (
        <>
            <GlobalStyles />
            <Switch>
                <ProtectedRoute path="/admin" component={Admin} type='admin'/>
                <Route exact path='/admin-auth/login' component={LoginPage} />
                <Route exact path="/rtl" component={RTL} />
                <Route exact path="/" component={Test} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/logout" render={() => logout} />
                {/* <Route exact path="/logout" component={Login} /> */}
                {/* <Redirect from="/" to="/admin/dashboard" /> */}
            </Switch>
        </>
    )
}

export default App

import React, { useState } from 'react'
import { Container as ContainerBase } from "components/misc/Layouts";
import tw from "twin.macro";
import styled from "styled-components";
import {css} from "styled-components/macro"; //eslint-disable-line
import illustration from "images/login.svg";
import logo from "images/logo.png";
import googleIconImageSrc from "images/google-icon.png";
import { ReactComponent as LoginIcon } from "feather-icons/dist/icons/log-in.svg";
import { gql, useMutation } from '@apollo/client';
import SyncLoader from "react-spinners/SyncLoader";
import Cookies from 'universal-cookie';
import {useHistory} from 'react-router-dom'
// import { login } from 'utils/auth';


const Container = tw(ContainerBase)`min-h-screen bg-mainBlue-200 text-white font-medium flex justify-center`;
const Content = tw.div`max-w-screen-xl m-0 sm:mx-20 sm:my-16 bg-white text-gray-900 shadow sm:rounded-lg flex justify-center flex-1`;
const MainContainer = tw.div`lg:w-1/2 xl:w-5/12 p-6 sm:p-12`;
const LogoLink = tw.a``;
const LogoImage = tw.img`h-12 mx-auto`;
const MainContent = tw.div`mt-12 flex flex-col items-center`;
const Heading = tw.h1`text-2xl xl:text-3xl font-extrabold`;
const FormContainer = tw.div`w-full flex-1 mt-8`;

const SocialButtonsContainer = tw.div`flex flex-col items-center`;
const SocialButton = styled.a`
  ${tw`w-full max-w-xs font-semibold rounded-lg py-3 border text-gray-900 bg-gray-100 hocus:bg-gray-200 hocus:border-gray-400 flex items-center justify-center transition-all duration-300 focus:outline-none focus:shadow-outline text-sm mt-5 first:mt-0`}
  .iconContainer {
    ${tw`bg-white p-2 rounded-full`}
  }
  .icon {
    ${tw`w-4`}
  }
  .text {
    ${tw`ml-4`}
  }
`;

const DividerTextContainer = tw.div`my-12 border-b text-center relative`;
const DividerText = tw.div`leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform -translate-y-1/2 absolute inset-x-0 top-1/2 bg-transparent`;
const Error = tw.div`text-red-500 text-center`
const Form = tw.form`mx-auto max-w-xs`;
const Input = tw.input`w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5 first:mt-0`;
const SubmitButton = styled.button`
  ${tw`mt-5 tracking-wide font-semibold bg-mainBlue-200 text-gray-100 w-full py-4 rounded-lg hover:bg-mainBlue-300 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none`}
  .icon {
    ${tw`w-6 h-6 -ml-2`}
  }
  .text {
    ${tw`ml-3`}
  }
`;
const IllustrationContainer = tw.div`sm:rounded-r-lg flex-1 bg-purple-100 text-center hidden lg:flex justify-center`;
const IllustrationImage = styled.div`
  ${props => `background-image: url("${props.imageSrc}");`}
  ${tw`m-12 xl:m-16 w-full max-w-lg bg-contain bg-center bg-no-repeat`}
`;

const LOGIN = gql`
mutation ($email: String! , $password: String!){
    login(email: $email, password: $password){
        user{
            fname
            lname
            role
        }
        token
    }
}
`

const Login = () => {
    const history = useHistory()
    const [login, { loading: mutationLoading, error: mutationError }] = useMutation(LOGIN)
    const [password, setPassword] = useState("")
    const [email, setEmail] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [error, setError] = useState(false)
    const cookies = new Cookies();

    const handleSubmit = (e) => {
        e.preventDefault()
        setError(false)
        setErrorMessage("")

        login({variables:{email:email, password:password}}).then(({data}) =>{
            const user = {'token': data.login.token, 'userType': data.login.user.role, 'fname': data.login.user.fname, 'lname': data.login.user.lname}
            
            cookies.set('user', user, {path:'/'})

            if(data.login.user.role === 'ADMIN'){
                history.push('/admin/dashboard')
            }else if(data.login.user.role === 'USER'){
                history.push('/user/dashboard')
            }else{
                history.push('/vendor/dashboard')
            }

        }).catch(error =>{
            setErrorMessage(error.message)
            setError(true)
            setPassword("")
        })
    }
    
    return (
        <Container>
            <Content>
                <MainContainer>
                    <LogoLink href="">
                        <LogoImage src={logo} />
                    </LogoLink>
                    <MainContent>
                        <Heading>Welcome to Beeki</Heading>
                        <FormContainer>
                            <SocialButtonsContainer>
                                
                                <SocialButton href="">
                                    <span className="iconContainer">
                                        <img src={googleIconImageSrc} className="icon" alt="" />
                                    </span>
                                    <span className="text">Login with google</span>
                                </SocialButton>
                                
                            </SocialButtonsContainer>
                            <DividerTextContainer>
                                <DividerText>Or Sign in with your e-mail</DividerText>
                            </DividerTextContainer>
                            <Form onSubmit={handleSubmit}>
                                {error ? <Error>{errorMessage}</Error> : ''}
                                <Input type="email" required value={email} placeholder="Email"  onChange={event => setEmail(event.target.value)}/>
                                <Input type="password" required value={password} placeholder="Password"  onChange={event => setPassword(event.target.value)}/>
                                {mutationLoading?  <SubmitButton disabled type="submit">
                                    <SyncLoader loading={true} color="#ffffff"/>
                                </SubmitButton> :  <SubmitButton type="submit">
                                    <LoginIcon className="icon" />
                                     <span className="text">login</span>
                                </SubmitButton>}
                               
                            </Form>
                            <p tw="mt-6 text-xs text-gray-600 text-center">
                                <a href="/forget-password" tw="border-b border-gray-500 border-dotted">
                                    Forgot Password ?
                                </a>
                            </p>
                            <p tw="mt-8 text-sm text-gray-600 text-center">
                                Dont have an account?{" "}
                                <a href="/register" tw="border-b border-gray-500 border-dotted">
                                    Sign Up
                                </a>
                            </p>
                        </FormContainer>
                    </MainContent>
                </MainContainer>
                <IllustrationContainer>
                    <IllustrationImage imageSrc={illustration} />
                </IllustrationContainer>
            </Content>
        </Container>
    )
}

export default Login

import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import loginPageStyle from 'assets/jss/pages/admin/loginStyle'
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Container from '@material-ui/core/Container';

const useStyles = makeStyles(loginPageStyle);

const LoginPage = () => {
    const classes = useStyles();

    return (
        <div className={classes.background}>
            <Container style={{marginTop: '50px'}}>
                <GridContainer alignItems="flex-end" justify="center">
                    <GridItem xs={12} sm={8} md={6} lg={4}>
                        <Card>
                            <CardHeader color="primary">
                                <h4 className={classes.cardTitleWhite}>Login to dashboard</h4>
                            </CardHeader>
                            <CardBody>
                                <GridContainer>
                                    <GridItem al xs={12} sm={12} md={12}>
                                        <CustomInput
                                            labelText="Enter your email"
                                            id="company-disabled"
                                            formControlProps={{
                                                fullWidth: true
                                            }}

                                            inputProps={{
                                                type: "email"
                                            }}
                                        />
                                    </GridItem>
                                    <GridItem xs={12} sm={12} md={12}>
                                        <CustomInput

                                            labelText="enter your password"
                                            id="username"
                                            formControlProps={{
                                                fullWidth: true
                                            }}

                                            inputProps={{
                                                type: "password"
                                            }}
                                        />
                                    </GridItem>
                                </GridContainer>
                            </CardBody>
                            <CardFooter>
                                <Button color="primary">Login</Button>
                            </CardFooter>
                        </Card>
                    </GridItem>
                </GridContainer>
            </Container>

        </div>
    )
}

export default LoginPage

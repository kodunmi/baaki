import React from 'react'
import "style.css"
import "tailwindcss/dist/base.css"
import AnimationRevealPage from "helpers/AnimationRevealPage"
import Hero from "components/hero/TwoColumnWithInput"
import MainFeature from "components/features/TwoColSingleFeatureWithStats2.js";
import MainFeature2 from "components/features/TwoColWithTwoFeaturesAndButtons.js";
import Header from 'components/headers/light'
import FeatureStats from "components/features/ThreeColCenteredStatsPrimaryBackground.js";
import Footer from 'components/footers/SimpleFiveColumn'
import FAQ from "components/faqs/SimpleWithSideImage.js";
import Cta from 'components/cta/DownloadApp'
import SliderCard from "components/cards/ThreeColSlider.js";
import ContactUsForm from "components/forms/TwoColContactUsWithIllustration.js";
import customerSupportIllustrationSrc from "images/customer-support-illustration.svg";
import tw from 'twin.macro'

const White = tw(MainFeature)`bg-white`;
const PaddingDiv = tw.div`py-5`
const Test = () => {
    return (
        <PaddingDiv>
        <Hero />
            {/* <White>
                <MainFeature textOnLeft={true}/>
            </White> */}
            <White textOnLeft={true}/>
            <MainFeature2/>
            <SliderCard/>
            <FAQ
            imageSrc={customerSupportIllustrationSrc}
            imageContain={true}
            imageShadow={false}
            subheading="FAQs"
            heading={
                <>
                Do you have <span tw="text-primary-500">Questions ?</span>
                </>
            }
            />
            <Cta/>
            <ContactUsForm/>
            <Footer/>
        </PaddingDiv>       
    )
}

export default Test
